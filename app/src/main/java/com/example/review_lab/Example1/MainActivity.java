package com.example.review_lab.Example1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.review_lab.R;

public class MainActivity extends AppCompatActivity {

    Cursor curMotorbike = null;

    MotorbikeAdapter adapter = null;
    // khai bao bien thanh vien lien quan den truy cap du lieu
    MotorbikeHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // khoi tao doi duong RestaurantHelper
        helper = new MotorbikeHelper(this);

        final ListView list = (ListView)findViewById(R.id.motorbike);
        list.setOnItemClickListener(onListClick);
        // lay du lieu tu CSDL
        curMotorbike = helper.getAll();
        startManagingCursor(curMotorbike);
        adapter = new MainActivity.MotorbikeAdapter(curMotorbike);
        list.setAdapter(adapter);

    }

    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            curMotorbike.moveToPosition(position);

           int CustomerId = helper.getId(curMotorbike);
            String CustomerName = helper.getCustomerName(curMotorbike);
            String MotorType = helper.getMotorType(curMotorbike);
            String MotorName = helper.getMotorName(curMotorbike);




           // CustomerId.setText(helper.getId(curMotorbike));


            Intent intent = new Intent(MainActivity.this, DetailMotorbike.class);

            intent.putExtra("CustomerId", CustomerId);
            intent.putExtra("CustomerName", CustomerName);
            intent.putExtra("MotorType", MotorType);
            intent.putExtra("MotorName", MotorName);

            startActivity(intent);
        } };

    //Ghi đè phương thức gán Menu cho Activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }


    //Ghi đè phương thức tạo sự kiện khi click vào item của menu
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        super.onOptionsItemSelected(item);

        /*    switch (item.getItemId()) {
            case R.id.add_motorbike:
                Intent intent = new Intent(this, AddMotorbike.class);
                startActivity(intent);
                break;
        } */

        if(item.getItemId()==R.id.add_motorbike) {
            //Toast.makeText(this, "Add clicked", Toast.LENGTH_LONG).show();

            //Chuyển màn hình sang Thêm Motorbike
            Intent intent = new Intent(MainActivity.this, AddMotorbike.class);
            startActivity(intent);
        }
        return true;
    }

    public class MotorbikeAdapter extends CursorAdapter {

        public MotorbikeAdapter(Cursor c) {
            super(MainActivity.this, c);
        }
        public MotorbikeAdapter(Context context, Cursor c) {
            super(context, c);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            // TODO Auto-generated method stub
            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.row, parent, false);
            return row;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            View row = view;
            ((TextView)row.findViewById(R.id.CustomerName)).setText(helper.getCustomerName(cursor));
            ((TextView)row.findViewById(R.id.MotorName)).setText(helper.getMotorName(cursor));
            ImageView icon = row.findViewById(R.id.icon);
            String MotorType = helper.getMotorType(cursor);
            if (MotorType.equals("Motorbike"))
                icon.setImageResource(R.drawable.motorbike);
            else
                icon.setImageResource(R.drawable.scooter);
        }
    }
}
