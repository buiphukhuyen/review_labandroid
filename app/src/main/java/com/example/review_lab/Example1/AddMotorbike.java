package com.example.review_lab.Example1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.*;

import com.example.review_lab.R;

public class AddMotorbike extends AppCompatActivity {

    Cursor curMotorbike = null;


    MotorbikeHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_motorbike);

        helper = new MotorbikeHelper(this);
        Button save = (Button) findViewById(R.id.btnAdd);
        EditText CustomerId = findViewById(R.id.edit_id);
        CustomerId.setEnabled(false);
        save.setOnClickListener(onSave);

    }

    private View.OnClickListener onSave = new View.OnClickListener() {
        public void onClick(View v) {
            try {

                Motorbike motorbike = new Motorbike();
                // tham chieu den cac view trong layout
                EditText CustomerName = findViewById(R.id.edit_Name);
                RadioGroup MotorTypes = findViewById(R.id.radiogr_MotorType);
                Spinner MotorName = findViewById(R.id.spiner_MotorName);

                motorbike.setCustomerName(CustomerName.getText().toString());

                switch (MotorTypes.getCheckedRadioButtonId())
                {
                    case R.id.type_Motorbike:
                        motorbike.setMotorType("Motorbike");
                        break;
                    case R.id.type_Scooter:
                        motorbike.setMotorType("Scooter");
                        break;
                    default:
                        break;
                }


                motorbike.setMotorName(MotorName.getSelectedItem().toString());

                if(helper.checkexists(motorbike.getMotorType(), motorbike.getMotorName())) {
                    Toast.makeText(AddMotorbike.this, "Xe đã được thuê", Toast.LENGTH_LONG).show();
                }
                else {
                    helper.insert(motorbike.getCustomerName(), motorbike.getMotorType(), motorbike.getMotorName());
                    //curMotorbike.requery();
                    Intent intent = new Intent(AddMotorbike.this, MainActivity.class);
                    startActivity(intent);
                }



            }catch (Exception e) {

            }

        }// end onClick
    }; // end View.OnClickListener

}
