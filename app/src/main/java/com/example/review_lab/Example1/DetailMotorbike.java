package com.example.review_lab.Example1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.review_lab.R;

public class DetailMotorbike extends AppCompatActivity {
    Cursor curMotorbike = null;

    MainActivity.MotorbikeAdapter adapter = null;

    MotorbikeHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_motor_bike);

        helper = new MotorbikeHelper(this);

        final Intent intent = getIntent();
        int Id = (intent.getIntExtra("CustomerId", 0));
        final String CustName = intent.getStringExtra("CustomerName");
        String MtType = intent.getStringExtra("MotorType");
        String MtName = intent.getStringExtra("MotorName");

        EditText CustomerId = findViewById(R.id.edit_id);
        EditText CustomerName = findViewById(R.id.edit_Name);
        Toast.makeText(this, "ID: " + Id, Toast.LENGTH_LONG).show();
        RadioGroup MotorType = findViewById(R.id.radiogr_MotorType);
        Spinner MotorName = findViewById(R.id.spiner_MotorName);

       //
        CustomerName.setText(CustName);
        CustomerId.setText(String.valueOf(Id));
        //CustomerId.setText(Id);
        CustomerId.setEnabled(false);
        if(MtType.equals("Motorbike"))
            MotorType.check(R.id.type_Motorbike);
        else
            MotorType.check(R.id.type_Scooter);

        MotorName.setSelection(getIndex(MotorName,MtName));

        Button btnEdit = findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Motorbike motorbike = new Motorbike();

                    // tham chieu den cac view trong layout
                    EditText CustomerId = findViewById(R.id.edit_id);
                    EditText CustomerName = findViewById(R.id.edit_Name);

                    RadioGroup MotorTypes = findViewById(R.id.radiogr_MotorType);
                    Spinner MotorName = findViewById(R.id.spiner_MotorName);

                    motorbike.setCustomerName(CustomerName.getText().toString());

                    switch (MotorTypes.getCheckedRadioButtonId())
                    {
                        case R.id.type_Motorbike:
                            motorbike.setMotorType("Motorbike");
                            break;
                        case R.id.type_Scooter:
                            motorbike.setMotorType("Scooter");
                            break;
                        default:
                            break;
                    }
                   // motorbike.setCustomerId(Integer.valueOf(CustomerId.getText());
                    motorbike.setCustomerId(Integer.valueOf(CustomerId.getText().toString()));
                    motorbike.setMotorName(MotorName.getSelectedItem().toString());


                    helper.update(motorbike.getCustomerId(), motorbike.getCustomerName(), motorbike.getMotorType(), motorbike.getMotorName());

                    Intent intent1 = new Intent(DetailMotorbike.this, MainActivity.class);
                    startActivity(intent1);

                }catch (Exception e) {
                    Log.e("LỖI 2", String.valueOf(e));
                }

            }

        });

        Button Delete = findViewById(R.id.btnDelete);
        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Motorbike motorbike = new Motorbike();
                EditText CustomerId = findViewById(R.id.edit_id);
                motorbike.setCustomerId(Integer.valueOf(CustomerId.getText().toString()));
                helper.delete(motorbike.getCustomerId());
                Toast.makeText(DetailMotorbike.this, "Deleted Success", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(DetailMotorbike.this, MainActivity.class);
                startActivity(intent);
            }
        });

        Button Caculator = findViewById(R.id.btnCal);
        Caculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int price = 0;
                EditText date = findViewById(R.id.edit_numberofdate);
                RadioGroup MotorTypes = findViewById(R.id.radiogr_MotorType);
                switch (MotorTypes.getCheckedRadioButtonId())
                {

                    case R.id.type_Motorbike:
                        price = 100000;
                        break;
                    case R.id.type_Scooter:
                        price = 200000;
                        break;
                    default:
                        break;
                }
                TextView NameDetail = findViewById(R.id.CustomerNameDetail);
                NameDetail.setText(CustName);
                Spinner MotorName = findViewById(R.id.spiner_MotorName);
                TextView MotorNameDetail = findViewById(R.id.MotorNameDetail);
                int Date = Integer.valueOf(date.getText().toString());
                MotorNameDetail.setText(MotorName.getSelectedItem().toString() + " - " + Date + " day");

                TextView Total = findViewById(R.id.Total);

                Total.setText("Total: " + date.getText().toString() + " x " + price + " = " + (Date*price));



            }
        });
    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }
        return 0;
    }
}
