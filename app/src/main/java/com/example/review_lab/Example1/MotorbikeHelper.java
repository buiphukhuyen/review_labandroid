package com.example.review_lab.Example1;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class MotorbikeHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "motorbike.db";
    private static final int SCHEMA_VERSION = 1;

    public MotorbikeHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA_VERSION);
    }

    public MotorbikeHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Motorbikes (_id INTEGER PRIMARY KEY AUTOINCREMENT, CustomerName TEXT, MotorName TEXT, MotorType TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public Cursor getAll() {
        Cursor cur;
        cur = getReadableDatabase().rawQuery("SELECT _id, CustomerName, MotorName, MotorType FROM Motorbikes", null);
        return (cur);
    }

    public int getId(Cursor c) {

        return (c.getInt(0));
    }

    public String getCustomerName(Cursor c) {
        return (c.getString(1));
    }

    public String getMotorName(Cursor c) { //cot

        return (c.getString(2));
    }

    public String getMotorType(Cursor c) {
        return (c.getString(3));
    }

    // phuong thuc insert mot dong thong tin nha hang
    public void insert(String CustomerName, String MotorType, String MotorName) {

        // tao doi tuong du lieu ContentValue
        ContentValues cv = new ContentValues();
        // dua cac du lieu vao theo tung cap ten field va value
        cv.put("CustomerName", CustomerName);
        cv.put("MotorType", MotorType);
        cv.put("MotorName", MotorName);
        // yeu cau SQLiteDatabase insert du lieu vao database
        getWritableDatabase().insert("Motorbikes", "CustomerName", cv);
    }
    public void update(int Id, String CustomerName, String MotorType, String MotorName) {
        try {
            String strFilter = "_id=" + Id;
            // tao doi tuong du lieu ContentValue
            ContentValues cv = new ContentValues();
            // dua cac du lieu vao theo tung cap ten field va value
            cv.put("CustomerName", CustomerName);
            cv.put("MotorType", MotorType);
            cv.put("MotorName", MotorName);
            // yeu cau SQLiteDatabase insert du lieu vao database
              getWritableDatabase().update("Motorbikes", cv, strFilter, null);
            getWritableDatabase().close();
        }
        catch (Exception e) {
            Log.e("LỖI", String.valueOf(e));
        }
    }

    public boolean delete(int Id)
    {
        String strFilter = "_id=" + Id;
        return getWritableDatabase().delete("Motorbikes", strFilter, null) > 0;
    }

    public boolean checkexists(String MotorType, String MotorName) {

        Cursor mCursor = getReadableDatabase().rawQuery("SELECT * FROM Motorbikes WHERE MotorType=? AND MotorName=?", new String[]{MotorType,MotorName});

       // Cursor cursor = null;
        //String checkQuery = "SELECT " + KEY_NAME + " FROM " + TABLE_SHOP + " WHERE " + KEY_NAME + "= '"+model.getName() + "'";
       // cursor= db.rawQuery(checkQuery,null);
        //boolean exists = (cursor.getCount() > 0);
        //cursor.close();

        Log.e("Cursor Count :", String.valueOf(mCursor.getCount()));

        if (mCursor.getCount() > 0)
        {
            return true;
            /* record exist */
        }
        else
        {
            return false;
            /* record not exist */
        }

    }
}
