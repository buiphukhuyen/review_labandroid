package com.example.review_lab.Example1;

public class Motorbike {
    private int CustomerId;
    private String CustomerName;
    private String MotorType;
    private String MotorName;

    public Motorbike() {}
    public Motorbike(int customerId, String customerName, String motorType, String motorName) {
        CustomerId = customerId;
        CustomerName = customerName;
        MotorType = motorType;
        MotorName = motorName;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getMotorType() {
        return MotorType;
    }

    public void setMotorType(String motorType) {
        MotorType = motorType;
    }

    public String getMotorName() {
        return MotorName;
    }

    public void setMotorName(String motorName) {
        MotorName = motorName;
    }
}
