package com.example.review_lab.Example2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "travel.db";
    private static final int SCHEMA_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA_VERSION);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Travel (_id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Address TEXT, Type TEXT, Country TEXT, Zipcode TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public Cursor getAll() {
        Cursor cur;
        cur = getReadableDatabase().rawQuery("SELECT _id, Name, Address, Type, Country, Zipcode FROM Travel", null);
        return (cur);
    }

    public int getId(Cursor c) {

        return (c.getInt(0));
    }

    public String getName(Cursor c) {
        return (c.getString(1));
    }

    public String getAddress(Cursor c) { //cot

        return (c.getString(2));
    }

    public String getType(Cursor c) {
        return (c.getString(3));
    }
    public String getCountry(Cursor c) {
        return (c.getString(4));
    }
    public int getZipcode(Cursor c) {
        return (c.getInt(5));
    }

    // phuong thuc insert mot dong thong tin nha hang
    public void insert(String Name, String Address, String Type, String Country, int Zipcode) {

        // tao doi tuong du lieu ContentValue
        ContentValues cv = new ContentValues();
        // dua cac du lieu vao theo tung cap ten field va value
        cv.put("Name", Name);
        cv.put("Address", Address);
        cv.put("Type", Type);
        cv.put("Country", Country);
        cv.put("Zipcode", Zipcode);
        // yeu cau SQLiteDatabase insert du lieu vao database
        getWritableDatabase().insert("Travel", "Name", cv);
    }
    public void update(int Id, String Name, String Address, String Type, String Country, int Zipcode) {
        try {
            String strFilter = "_id=" + Id;
            // tao doi tuong du lieu ContentValue
            ContentValues cv = new ContentValues();
            // dua cac du lieu vao theo tung cap ten field va value
            cv.put("Address", Address);
            cv.put("Type", Type);
            cv.put("Country", Country);
            cv.put("Zipcode", Zipcode);
            // yeu cau SQLiteDatabase insert du lieu vao database
            getWritableDatabase().update("Travel", cv, strFilter, null);
            getWritableDatabase().close();
        }
        catch (Exception e) {
            Log.e("LỖI", String.valueOf(e));
        }
    }

    public boolean delete(int Id)
    {
        String strFilter = "_id=" + Id;
        return getWritableDatabase().delete("Travel", strFilter, null) > 0;
    }

    public boolean checkexists(String MotorType, String MotorName) {

        Cursor mCursor = getReadableDatabase().rawQuery("SELECT * FROM Travel WHERE MotorType=? AND MotorName=?", new String[]{MotorType,MotorName});

        // Cursor cursor = null;
        //String checkQuery = "SELECT " + KEY_NAME + " FROM " + TABLE_SHOP + " WHERE " + KEY_NAME + "= '"+model.getName() + "'";
        // cursor= db.rawQuery(checkQuery,null);
        //boolean exists = (cursor.getCount() > 0);
        //cursor.close();

        Log.e("Cursor Count :", String.valueOf(mCursor.getCount()));

        if (mCursor.getCount() > 0)
        {
            return true;
            /* record exist */
        }
        else
        {
            return false;
            /* record not exist */
        }

    }
}
