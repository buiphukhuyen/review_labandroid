package com.example.review_lab.Example2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.review_lab.Example1.MotorbikeHelper;
import com.example.review_lab.R;

import java.util.ArrayList;

public class InsertActivity extends AppCompatActivity {

    Cursor curTravel= null;


    DBHelper helper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        helper = new DBHelper(this);

        final EditText Name = findViewById(R.id.edit_Names);
        final EditText Address = findViewById(R.id.edit_Address);
        final RadioGroup Type = findViewById(R.id.radio_group);
        final Spinner Country = findViewById(R.id.spiner_Country);
        final EditText Zipcode = findViewById(R.id.edit_Zipcode);

        final RadioButton rdTN = findViewById(R.id.radio_TrongNuoc);
        final RadioButton rdNN = findViewById(R.id.radio_NgoaiNuoc);

        Zipcode.setEnabled(false);

        Type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(rdTN.isChecked()) {
                 //   Toast.makeText(InsertActivity.this, "Clicked Trong nước", Toast.LENGTH_LONG).show();
                    String[] testArray = getResources().getStringArray(R.array.listCountryVN);
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(InsertActivity.this, android.R.layout.simple_spinner_item, testArray );
                    Country.setAdapter(spinnerArrayAdapter);
                }
                else if(rdNN.isChecked()){
                  //  Toast.makeText(InsertActivity.this, "Clicked Ngoài nước", Toast.LENGTH_LONG).show();
                    String[] testArray = getResources().getStringArray(R.array.listCountryNN);
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(InsertActivity.this, android.R.layout.simple_spinner_item, testArray );
                    Country.setAdapter(spinnerArrayAdapter);
                }
            }
        });
        Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(rdTN.isChecked()) {
                    String[] testArray = getResources().getStringArray(R.array.listZipcodeVN);
                    Zipcode.setText(testArray[position]);
                }
                else {
                    String[] testArray = getResources().getStringArray(R.array.listZipcodeNN);
                    Zipcode.setText(testArray[position]);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button btnInsert = findViewById(R.id.btnInsert);

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Information information = new Information();
                information.setName(Name.getText().toString());
                information.setAddress(Address.getText().toString());
               if(Type.getCheckedRadioButtonId() == R.id.radio_NgoaiNuoc)
                   information.setType("Ngoai Nuoc");
               else
                   information.setType("Trong Nuoc");

               information.setCountry(Country.getSelectedItem().toString());
               information.setZipcode(Integer.valueOf(Zipcode.getText().toString()));


               helper.insert(information.getName(), information.getAddress(), information.getType(), information.getCountry(), information.getZipcode());

               Toast.makeText(InsertActivity.this,"Country đã thêm" + information.getCountry(), Toast.LENGTH_LONG).show();
                Toast.makeText(InsertActivity.this,"Zipcode đã thêm" + information.getZipcode(), Toast.LENGTH_LONG).show();
               Intent intent = new Intent(InsertActivity.this, HomeActivity.class);
               startActivity(intent);

            }
        });


    }


}
