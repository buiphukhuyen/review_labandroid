package com.example.review_lab.Example2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.review_lab.Example1.DetailMotorbike;
import com.example.review_lab.Example1.MainActivity;
import com.example.review_lab.Example1.MotorbikeHelper;
import com.example.review_lab.R;


public class HomeActivity extends AppCompatActivity {
    Cursor curTravel = null;
    TravelAdapter adapter = null;

    // khai bao bien thanh vien lien quan den truy cap du lieu
    DBHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        helper = new DBHelper(this);

        final ListView list = findViewById(R.id.LstMain);
        list.setOnItemClickListener(onListClick);
        // lay du lieu tu CSDL
        curTravel = helper.getAll();
        startManagingCursor(curTravel);
        adapter = new HomeActivity.TravelAdapter(curTravel);
        list.setAdapter(adapter);
    }
    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            curTravel.moveToPosition(position);

            int Id = helper.getId(curTravel);
            String Name = helper.getName(curTravel);
            String Address = helper.getAddress(curTravel);
            String Type = helper.getType(curTravel);
            String Country = helper.getCountry(curTravel);
            int Zipcode = helper.getZipcode(curTravel);


           // Toast.makeText(HomeActivity.this,"Country:" + Country, Toast.LENGTH_LONG).show();
         //   Toast.makeText(HomeActivity.this,"Zip:" + Zipcode, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(HomeActivity.this, DetailTravelActivity.class);
            intent.putExtra("Id", Id);
            intent.putExtra("Name", Name);
            intent.putExtra("Address", Address);
            intent.putExtra("Type", Type);
            intent.putExtra("Country", Country);
            intent.putExtra("Zipcode", Zipcode);

            startActivity(intent);
        } };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.add_motorbike) {
            Intent intent = new Intent(this, InsertActivity.class);
            startActivity(intent);
        }
        return true;
    }

    public class TravelAdapter extends CursorAdapter {

        public TravelAdapter(Cursor c) {
            super(HomeActivity.this, c);
        }
        public TravelAdapter(Context context, Cursor c) {
            super(context, c);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            // TODO Auto-generated method stub
            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.rows, parent, false);
            return row;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            View row = view;
            ((TextView)row.findViewById(R.id.Name)).setText(helper.getName(cursor));
            ((TextView)row.findViewById(R.id.Address)).setText(helper.getAddress(cursor));
            ImageView icon = row.findViewById(R.id.icons);
            String MotorType = helper.getType(cursor);
            if (MotorType.equals("Trong Nuoc"))
                icon.setImageResource(R.drawable.motorbike);
            else
                icon.setImageResource(R.drawable.scooter);
        }
    }
}
