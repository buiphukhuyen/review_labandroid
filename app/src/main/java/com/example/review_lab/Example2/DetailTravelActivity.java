package com.example.review_lab.Example2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.review_lab.Example1.DetailMotorbike;
import com.example.review_lab.Example1.MainActivity;
import com.example.review_lab.Example1.MotorbikeHelper;
import com.example.review_lab.R;

public class DetailTravelActivity extends AppCompatActivity {

    Cursor curTravel = null;

    HomeActivity.TravelAdapter adapter = null;

    DBHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_travel);

        helper = new DBHelper(this);

        final Intent intent = getIntent();
        final int Id = intent.getIntExtra("Id", 0);
        final String Name = intent.getStringExtra("Name");
        final String Address = intent.getStringExtra("Address");
        final String Type = intent.getStringExtra("Type");
        final String Country = intent.getStringExtra("Country");
        String Zipcode = intent.getStringExtra("Zipcode");

        final EditText Name2 = findViewById(R.id.edit_Names);
        final EditText Address2 = findViewById(R.id.edit_Address);
        final RadioGroup Type2 = findViewById(R.id.radio_group);
        final Spinner Country2 = findViewById(R.id.spiner_Country);
        final EditText Zipcode2 = findViewById(R.id.edit_Zipcode);

        final RadioButton rdTN = findViewById(R.id.radio_TrongNuoc);
        final RadioButton rdNN = findViewById(R.id.radio_NgoaiNuoc);

        Zipcode2.setEnabled(false);

        Name2.setText(Name);
        Address2.setText(Address);
        if(Type.equals("Trong Nuoc")) {
            rdTN.setChecked(true);
            String[] testArray = getResources().getStringArray(R.array.listCountryVN);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(DetailTravelActivity.this, android.R.layout.simple_spinner_item, testArray );
            Country2.setAdapter(spinnerArrayAdapter);
        }
        else {
            rdNN.setChecked(true);
            String[] testArray = getResources().getStringArray(R.array.listCountryNN);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(DetailTravelActivity.this, android.R.layout.simple_spinner_item, testArray );
            Country2.setAdapter(spinnerArrayAdapter);
        }

        Type2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(rdTN.isChecked()) {
                    //   Toast.makeText(InsertActivity.this, "Clicked Trong nước", Toast.LENGTH_LONG).show();
                    String[] testArray = getResources().getStringArray(R.array.listCountryVN);
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(DetailTravelActivity.this, android.R.layout.simple_spinner_item, testArray );
                    Country2.setAdapter(spinnerArrayAdapter);
                }
                else if(rdNN.isChecked()){
                    //  Toast.makeText(InsertActivity.this, "Clicked Ngoài nước", Toast.LENGTH_LONG).show();
                    String[] testArray = getResources().getStringArray(R.array.listCountryNN);
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(DetailTravelActivity.this, android.R.layout.simple_spinner_item, testArray );
                    Country2.setAdapter(spinnerArrayAdapter);
                }
            }
        });
        Country2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(rdTN.isChecked()) {
                    String[] testArray = getResources().getStringArray(R.array.listZipcodeVN);
                    Zipcode2.setText(testArray[position]);
                }
                else {
                    String[] testArray = getResources().getStringArray(R.array.listZipcodeNN);
                    Zipcode2.setText(testArray[position]);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Toast.makeText(DetailTravelActivity.this,"Country:" + Country, Toast.LENGTH_LONG).show();


        Country2.setSelection(getIndex(Country2, Country));

        Zipcode2.setText(Zipcode);

        Button btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Information information = new Information();
                information.setName(Name2.getText().toString());
                information.setAddress(Address2.getText().toString());
                if(Type2.getCheckedRadioButtonId() == R.id.radio_NgoaiNuoc)
                    information.setType("Ngoai Nuoc");
                else
                    information.setType("Trong Nuoc");

                information.setCountry(Country2.getSelectedItem().toString());
                information.setZipcode(Integer.valueOf(Zipcode2.getText().toString()));


                helper.update(Id, information.getName(), information.getAddress(), information.getType(), information.getCountry(), information.getZipcode());

                Intent intent = new Intent(DetailTravelActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        Button btnDel = findViewById(R.id.btnDel);
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helper.delete(Id);
                Toast.makeText(DetailTravelActivity.this, "Deleted Success", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(DetailTravelActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

    }

    //Lấy index covert sang String
    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }
        return 0;
    }

}
